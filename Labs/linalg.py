import numpy as np

def dot_prod(u,v):
    '''
    vector product
    
    input: two vectors of equal length
    output: scalar 
        
    '''
    if len(u) is not len(v): 
        return("Error: vectors do not have same length.")
    
    return sum(np.array(u)*np.array(v));



def gausselim(A,u):
    '''Gaussian elimination with backsubstution
    
    This function solves the equation u = A * v, where v, u vectors 
    of length N and A a NxN matrix.
    
    input: A, u
    output solution vector v
    '''
    if len(A.T) is not len(u): #if the lengths are different
        return("Error: vectors do not have same length.")
    
    #set up work copy of A, dtype = float, and add RHS
    AA = vstack((A.T,u)).T 
    if not 'float' in str(AA.dtype):
        AA=AA.astype(float) 
    
    #iter over rows from first (index 0) to second to last(index len(A) - 1)
    for i in range(len(AA)):
        TOL = 1*E-10
        if abs(AA[i,i]) < TOL:
            for k in range(i+1, len(AA)):
                if abs(AA[k,k]) > TOL:
                    row_copy = array(AA[i])
                    AA[i] = AA[k]
                    AA[k] = row_copy
        
        AA[i] = AA[i]/AA[i,i]
        for j in range(i+1,len(AA)):
            AA[j] -= AA[j,i]*AA[i]
    
    #back substitution
    BB = copy(AA)
    m  = len(A)-1     # highest row/col index (we do only square matrices)
    v  = zeros(len(u),dtype = float) 
    u  = np.asarray(BB.T[-1]).flatten()       #extract RHS
    AA = delete(BB,m+1,1) # recover diagonalized coefficient matrix  
    
    #dot = lambda x,y: sum(x[i]*y[i] for i in range(len(x)))
    
    for j in range(m,-1,-1):
        v[j] = u[j] - v.dot(AA[j]) #dot(v, np.asarray(AA[j]).flatten())
        
    return v;