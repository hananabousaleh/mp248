def plotNewt(f, x, x0, n):
    '''
    Plots Newton's Method for a given function and initial value.
    
    parameters:
        f - sympy function of x
        x - variable name (sympy symbol)
        x0 - variable initial value (float)
        n - max number of iterations (int)
    '''
    
    N = x - f/f.diff(x)
    IT = sp.lambdify(x, N)
    F = sp.lambdify(x, f)
    
    plt.close() ## close previous plot
    fig, ax = plt.subplots(figsize = (12,7))

    xl = [x0]
    yl = [0.0]
    for i in range(n):
        xl.append(xl[-1])
        yl.append(F(xl[-1]))
        xl.append(IT(xl[-1]))
        yl.append(0.0)
    
    dom = np.linspace(min(xl)-0.5, max(xl)+0.5, 1000)
    plt.plot(dom, [F(x) for x in dom], '-', label="$"+sp.latex(f)+"$")
    plt.plot(dom, [0.0 for x in dom], color="black",label="$y=0$")
    plt.plot([x0], [0.0], 'ro', label="init")
    plt.plot(xl,yl, color="orange", label="Newt")
    
    legend = ax.legend(loc='upper left')

    plt.show()