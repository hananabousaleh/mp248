import numpy as np

def mean(x):
    '''
    calculates the mean of a series of numbers
    
    parameter:
    
    x: array or list of numbers
    '''
    
    n = len(x)
    return np.sum(x)/n

def var(x):
    '''
    calculates the variance of a series of numbers, calls the mean function.
    
    parameter:
    
    x: array or list of numbers
    '''
    
    n = len(x) -1
    mu = mean(x)
    
    if n==0.:
        return("Invalid sample variance: only one element given")
    
    return sum((x-mu)**2)/n

def skew(x):
    '''
    calculates the skew of a series of numbers, calls the mean function and var function.
    
    parameter:
    
    x: array or list of numbers
    
    '''
    
    n = len(x)
    mu = mean(x)
    sigma = math.sqrt(var(x))
    if sigma == 0.:
        return "Invalid skew, standard deviation/variance is zero"
    
    return (sum(((x-mu)/sigma)**3)/n)

def pear_coeff(x,y):
    '''
    calculates the Pearson's r coefficient for N pairs of values.
        calls mean function.
    
    parameters:
    x, y : arrays/lists, the two sets of values we want to calculate the coefficient for.
        must be the same length
    
    returns: the correlation coefficient.
    '''
    x = np.array(x)
    y = np.array(y)
    
    if len(x) != len(y):
        return "arrays are different lengths."
    xbar = x - mean(x)
    ybar = y - mean(y)
    
    if (sum(xbar) == 0):
        return "division by zero, yikers.";
    
    if (sum(ybar) == 0):
        return "division by zero, yikers.";
    
    
    numerator = sum(xbar*ybar)
    
    xbars = (xbar)**2
    ybars = (ybar)**2
    
    denominator = sqrt(sum(xbars))*sqrt(sum(ybars))
    
    return numerator/denominator;
